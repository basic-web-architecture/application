**Architecture**

![Web app architecture](/app-architecture.drawio.svg "Web app architecture")

**Initialization**

After cloning the run , run `git submodule update --init --recursive --merge`
If on Windows, correct the EOL conversion of entrypoint.sh

**How to:**

The whole application can be run in 1 of the 4 ways mentioned below:

1. Using Docker-Compose
The docker compose file will bring up both the containeres in the same network. The application will be up in `localhost` The advantage of this approach is the application can be easily deployed using beanstalk, etc. The disadvantage being, the front end and the backend will lie in the same layer, thus making both the tiers separate impossible.

2. Using docker image from production builds
The commands `npm run image-build` and `npm run image-run` will build and run the production builds for the frontend and backend projects respectively. The application will be up in `localhost`

3. Using development build
The commands `ng serve --open` and `node index` will serve the frontend and backend respectively. The application will be up in `localhost:4200`

4. Using public IP addresses (for example, in AWS ECS)
While running the frontend docker container using run command or using TaskDefinitions in ECS, give the environment variable backend=<Backend_IP_Address>
While running the backend docker container using run command or using TaskDefinitions in ECS, give the environment variable origin=<Frontend_IP_Address>

**AWS deployment**

While creating task definition for frontend, give the environment variable as backend=<BACKEND_ELASTIC_IP_ADDRESS>.
Similarly, for backend, give the environment variable as origin=<FRONTEND_ELASTIC_IP_ADDRESS>.
Create network load balancers for frontend and backend and assign them the corresponding elastic IP addresses.
Create the services for frontend and backend with the network load balancers. Once the backend service is created, add a security rule to the security group of the backend service with port 3000 and source as the security group of the frontend.
Also, while creating the Network load balancer for backend, modify the healthcheck port to be 3000.

**Help**

Command to build an image: 
`$docker image build -t <IMAGE_NAME>:<IMAGE_TAG> .`

Command example with my image: 
`$docker image build -t simple-app-image .`

Command to tag an image: 
`$docker image tag <IMAGE_NAME>:<IMAGE_TAG>  <REPOSITORY_URI>:<IMAGE_TAG>`

Command example with my image and repository: 
`$docker image tag simple-app-image:latest  708995052028.dkr.ecr.us-east-2.amazonaws.com/ecr-tutorial-image:latest`

Command: 
`$aws ecr get-login-password --region <REPOSITORY_REGION> | docker login --username AWS --password-stdin <REPOSITORY_URI>`

Command example with my region and repository: 
`$aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 708995052028.dkr.ecr.us-east-1.amazonaws.com`

Command: 
`$docker image push <IMAGE_NAME[:TAG]>`

Example with an image: 
`$docker image push 708995052028.dkr.ecr.us-east-2.amazonaws.com/ecr-tutorial-image:latest`
